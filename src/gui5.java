import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;


public class gui5 {
	public static void main(String[]args){
		new gui5();
	}
	public gui5(){
		JFrame frame = new JFrame();
		
		frame.setTitle("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		
		
		JMenuBar menuBar = new JMenuBar();
		frame.add(menuBar);
		JMenu fileMenu = new JMenu("File");
		JMenu editMenu = new JMenu("Edit");
		//menuBar.add(fileMenu);
		menuBar.add(editMenu);
		
		panelsouth.setLayout(new GridLayout(5,4));
		JMenuItem red = new JMenuItem("Red");
		JMenuItem blue = new JMenuItem("Blue");
		JMenuItem green = new JMenuItem("Green");
		
		JCheckBoxMenuItem checkaction = new JCheckBoxMenuItem("check");
		JRadioButtonMenuItem action1 = new JRadioButtonMenuItem("Check1");
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(fileMenu);
		bg.add(editMenu);
		editMenu.add(red);
		editMenu.add(green);
		editMenu.add(blue);
		
		//panelsouth.add(editMenu);
		
		
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.RED);
			
			}
		
		
		});
		
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.GREEN);
			
			}
		
		
		});
		
		blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				panelcenter.setBackground(Color.BLUE);
			}
		});
		
		
		frame.add(panelcenter,BorderLayout.NORTH);
		
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		frame.setVisible(true);

	}
	
}
