import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class gui4 {
	public static void main(String[]args){
		new gui4();
	}
	public gui4(){
		
		JFrame frame = new JFrame();
		
		frame.setTitle("Test4");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		
		
		String checkbox1[] = {"red","blue","green"};
		JComboBox<String> box1 = new JComboBox<String>(checkbox1);
		box1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(box1.getSelectedItem().equals("red")){
					panelcenter.setBackground(Color.red);
				}
				else if(box1.getSelectedItem().equals("blue")){
					panelcenter.setBackground(Color.blue);
				}
				else if(box1.getSelectedItem().equals("green")){
					panelcenter.setBackground(Color.green);
				}
			}
			
		});
		
		panelsouth.add(box1);
		
		frame.add(panelcenter,BorderLayout.CENTER);
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		
		
		
		
		frame.setVisible(true);
		
		
		
	}


}
