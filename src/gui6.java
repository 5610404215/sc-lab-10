import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class gui6 {
	public static void main(String[]args){
		new gui6();
	}
	public gui6(){
		JFrame frame = new JFrame();
		controller control = new controller();
		
		frame.setTitle("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		//JButton red = new JButton("Red");
		JButton deposit = new JButton("deposit");
		JButton withdraw = new JButton("withdraw");
		JLabel output = new JLabel("The Result will show here");
		
		JTextField field = new JTextField(20);
		//panelsouth.add(red);
		
		panelsouth.add(output);
	
		
		panelcenter.add(field);
		panelcenter.add(deposit);
		panelcenter.add(withdraw);
		
		deposit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				output.setText(Double.toString((control.deposit(Double.parseDouble(field.getText())))));
			}
				
			
		});
		
		withdraw.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				output.setText(Double.toString((control.withdraw(Double.parseDouble(field.getText())))));
			}
				
			
		});
		
		
		
		
		frame.add(panelcenter,BorderLayout.CENTER);
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		
		
		
		
		frame.setVisible(true);
		

	}
	
}
