
public class model {
	 private double balance;

	   /**
	      Constructs a bank account with a zero balance.
	   */
	   public model()
	   {   
	      balance = 0;
	   }

	   /**
	      Constructs a bank account with a given balance.
	      @param initialBalance the initial balance
	   */
	   public model(double initialBalance)
	   {   
	      balance = initialBalance;
	   }

	   /**
	      Deposits money into the bank account.
	      @param amount the amount to deposit
	   */
	   public double deposit(double amount)
	   {  
	      double newBalance = balance + amount;
	      return balance = newBalance;
	   }

	   /**
	      Withdraws money from the bank account.
	      @param amount the amount to withdraw
	   */
	   public double withdraw(double amount)
	   {   
	      double newBalance = balance - amount;
	      return balance = newBalance;
	   }

	   /**
	      Gets the current balance of the bank account.
	      @return the current balance
	   */
	   public double getBalance()
	   {   
	      return balance;
	   }

}
