import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class gui1 {
	public static void main(String[]args){
		new gui1();
	}
	public gui1(){
		JFrame frame = new JFrame();
		
		frame.setTitle("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		JButton red = new JButton("Red");
		JButton green = new JButton("Green");
		JButton blue = new JButton("Blue");
		panelsouth.add(red);
		panelsouth.add(blue);
		panelsouth.add(green);
		
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.RED);
			
			}
		
		
		});
		
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.GREEN);
			
			}
		
		
		});
		
		blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				panelcenter.setBackground(Color.BLUE);
			}
		});
		
		
		
		
		
		frame.add(panelcenter,BorderLayout.CENTER);
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		
		
		
		
		frame.setVisible(true);
		
		
		
	}

}
