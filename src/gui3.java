import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class gui3 {
	public static void main(String[]args){
		new gui3();
	}
	public gui3(){
		JFrame frame = new JFrame();
		
		frame.setTitle("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		JButton check = new JButton("Check");
		JCheckBox red = new JCheckBox("Red");
		JCheckBox blue = new JCheckBox("Blue");
		JCheckBox green = new JCheckBox("Green");
		
		panelsouth.setLayout(new GridLayout(5,4));
		panelsouth.add(red);
		panelsouth.add(blue);
		panelsouth.add(green);
		panelsouth.add(check);
		
		check.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(red.isSelected()){
					panelcenter.setBackground(Color.RED);
					if(green.isSelected()){
						panelcenter.setBackground(Color.YELLOW);
					}
					if(blue.isSelected()){
						panelcenter.setBackground(Color.MAGENTA);
					}
					
				}
				if(blue.isSelected()){
					panelcenter.setBackground(Color.BLUE);
					if(green.isSelected()){
						panelcenter.setBackground(Color.YELLOW);
					}
					if(red.isSelected()){
						panelcenter.setBackground(Color.MAGENTA);
					}
					
				}
				if(green.isSelected()){
					panelcenter.setBackground(Color.GREEN);
					if(red.isSelected()){
						panelcenter.setBackground(Color.YELLOW);
					}
					if(blue.isSelected()){
						panelcenter.setBackground(Color.cyan);
					}
					
				}
			}
			
		});
		
		frame.add(panelcenter,BorderLayout.CENTER);
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		frame.setVisible(true);
		
		
		
	}

}
