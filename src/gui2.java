import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class gui2 {
	public static void main(String[]args){
		new gui2();
	}
	public gui2(){
		JFrame frame = new JFrame();
		
		frame.setTitle("Test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		  
		frame.pack();
		frame.setSize(800, 700);
		frame.setLocation(10, 10);
		frame.setLayout(new BorderLayout());
		JPanel panelsouth = new JPanel();
		JPanel panelcenter = new JPanel();
		
		JRadioButton red = new JRadioButton("Red");
//		red.setMnemonic(KeyEvent.VK_R);
//		red.setActionCommand("redString");
		
		JRadioButton green = new JRadioButton("Green");
		JRadioButton blue = new JRadioButton("Blue");
		
		ButtonGroup group = new ButtonGroup();
		group.add(red);
		panelsouth.add(red);
		group.add(green);
		panelsouth.add(green);
		group.add(blue);
		panelsouth.add(blue);
		
		
		
		
		
		
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.RED);
			
			}
		
		
		});
		
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelcenter.setBackground(Color.GREEN);
			
			}
		
		
		});
		
		blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				panelcenter.setBackground(Color.BLUE);
			}
		});
		
		
		frame.add(panelcenter,BorderLayout.CENTER);
		frame.add(panelsouth,BorderLayout.SOUTH);
		
		frame.setVisible(true);
		
		
		
	}


}
